package expendiente.clinico.models.entitys;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Entity
@Table(name = "Clasificacion_IMC", schema = "catalogo")
public class ClasificacionIMC implements Serializable {

	private static final long serialVersionUID = 68622452L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "IdClasificacion_IMC")
	private Long id;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
	@NotBlank(message = "El nombre de Clasificacion IMC no puede quedar vacio")
	@Basic(optional = false)
	@Column(name = "NombreClasificacion_IMC", unique = true)
	private String nombreClasificacionIMC;

	@NotNull(message = "El valor mínimo no puede quedar vacio")
	@Basic(optional = false)
	@Column(name = "ValorMinimo")
	private BigDecimal valorMinimo;

	@NotNull(message = "El valor máximo no puede quedar vacio")
	@Basic(optional = false)
	@Column(name = "ValorMaximo")
	private BigDecimal valorMaximo;

	@NotNull(message = "El estado clasificacionIMC no puede ser null")
	@Basic(optional = false)
	@Column(name = "EstadoClasificacion_IMC")
	private Boolean estadoClasificacionIMC;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚ][a-zA-ZáéíóúÁÉÍÓÚ ]{0,23}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 24 caracteres")
	@Column(name = "ColorClasificacion_IMC")
	private String colorClasificacionIMC;

	public ClasificacionIMC(Long id,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El nombre de Clasificacion IMC no puede quedar vacio") String nombreClasificacionIMC,
			@NotNull(message = "El valor mínimo no puede quedar vacio") BigDecimal valorMinimo,
			@NotNull(message = "El valor máximo no puede quedar vacio") BigDecimal valorMaximo,
			@NotNull(message = "El estado clasificacionIMC no puede ser null") Boolean estadoClasificacionIMC,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚ][a-zA-ZáéíóúÁÉÍÓÚ ]{0,23}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 24 caracteres") String colorClasificacionIMC) {
		super();
		this.id = id;
		this.nombreClasificacionIMC = nombreClasificacionIMC;
		this.valorMinimo = valorMinimo;
		this.valorMaximo = valorMaximo;
		this.estadoClasificacionIMC = estadoClasificacionIMC;
		this.colorClasificacionIMC = colorClasificacionIMC;
	}

	public ClasificacionIMC() {
		super();
	}
	
}
