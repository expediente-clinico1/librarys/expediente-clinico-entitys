package expendiente.clinico.models.entitys;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "TipoEnfermedad", schema = "catalogo")
public class TipoEnfermedad implements Serializable {

	private static final long serialVersionUID = 185239271L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "IdTipoEnfermedad")
	private Long id;

	@NotNull(message = "El estado tipo de enfermedad no puede ser null")
	@Basic(optional = false)
	@Column(name = "EstadoTipoEnfermedad")
	private Boolean estadoTipoEnfermedad;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
	@NotBlank(message = "El nombre del tipo enfermedad no puede quedar vacio")
	@Basic(optional = false)
	@Column(name = "NombreTipoEnfermedad")
	private String nombreTipoEnfermedad;

	@JsonIgnore
	@OneToMany(mappedBy = "idTipoEnfermedad")
	private List<Enfermedad> enfermedadList;

	public TipoEnfermedad(Long id,
			@NotNull(message = "El estado tipo de enfermedad no puede ser null") Boolean estadoTipoEnfermedad,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El nombre del tipo enfermedad no puede quedar vacio") String nombreTipoEnfermedad) {
		super();
		this.id = id;
		this.estadoTipoEnfermedad = estadoTipoEnfermedad;
		this.nombreTipoEnfermedad = nombreTipoEnfermedad;
	}

	public TipoEnfermedad() {
		super();
	}

}
