package expendiente.clinico.models.entitys;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Entity(name = "Enfermedad")
@Table(name = "Enfermedad", schema = "catalogo")
public class Enfermedad implements Serializable {

	private static final long serialVersionUID = 454050031L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "IdEnfermedad")
	private Long id;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
	@NotBlank(message = "El nombre de enfermedad no puede quedar vacio")
	@Basic(optional = false)
	@Column(name = "NombreEnfermedad", unique = true)
	private String nombreEnfermedad;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,126}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 127 caracteres")
	@Column(name = "DescripcionEnfermedad")
	private String descripcionEnfermedad;

	@NotNull(message = "El estado de la enfermedad no debe de ser null")
	@Basic(optional = false)
	@Column(name = "EstadoEnfermedad")
	private Boolean estadoEnfermedad;

	@JoinColumn(name = "IdTipoEnfermedad", referencedColumnName = "IdTipoEnfermedad", foreignKey = @ForeignKey(name = "FK_Enfermedad_TipoEnfermedad"))
	@ManyToOne(optional = false, targetEntity = TipoEnfermedad.class)
	private TipoEnfermedad idTipoEnfermedad;

	public Enfermedad(Long id,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El nombre de enfermedad no puede quedar vacio") String nombreEnfermedad,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,126}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 127 caracteres") String descripcionEnfermedad,
			@NotNull(message = "El estado de la enfermedad no debe de ser null") Boolean estadoEnfermedad,
			TipoEnfermedad idTipoEnfermedad) {
		super();
		this.id = id;
		this.nombreEnfermedad = nombreEnfermedad;
		this.descripcionEnfermedad = descripcionEnfermedad;
		this.estadoEnfermedad = estadoEnfermedad;
		this.idTipoEnfermedad = idTipoEnfermedad;
	}

	public Enfermedad() {
		super();
	}

}
