package expendiente.clinico.models.entitys;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Entity
@Table(name = "Municipio", schema = "catalogo")
public class Municipio implements Serializable {
	private static final long serialVersionUID = 600102009L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "IdMunicipio")
	private Long id;

	@NotNull(message = "El estado municipio no puede ser null")
	@Basic(optional = false)
	@Column(name = "EstadoMunicipio")
	private Boolean estadoMunicipio;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ][a-zA-ZáéíóúÁÉÍÓÚñÑ ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
	@NotBlank(message = "El nombre del municipio no puede quedar vacio")
	@Basic(optional = false)
	@Column(name = "NombreMunicipio", unique = true)
	private String nombreMunicipio;

	@JoinColumn(name = "IdDepartamento", referencedColumnName = "IdDepartamento",foreignKey=@ForeignKey(name="FK_Municipio_Departamento"))
	@ManyToOne(optional = false, targetEntity = Departamento.class)
	private Departamento idDepartamento;

	public Municipio(Long id, @NotNull(message = "El estado municipio no puede ser null") Boolean estadoMunicipio,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ][a-zA-ZáéíóúÁÉÍÓÚñÑ ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El nombre del municipio no puede quedar vacio") String nombreMunicipio,
			Departamento idDepartamento) {
		super();
		this.id = id;
		this.estadoMunicipio = estadoMunicipio;
		this.nombreMunicipio = nombreMunicipio;
		this.idDepartamento = idDepartamento;
	}

	public Municipio() {
		super();
	}

}
