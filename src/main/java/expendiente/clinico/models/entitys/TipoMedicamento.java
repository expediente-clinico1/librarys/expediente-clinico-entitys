package expendiente.clinico.models.entitys;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "TipoMedicamento", schema = "catalogo")
public class TipoMedicamento implements Serializable {

	private static final long serialVersionUID = 391926334L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "IdTipoMedicamento")
	private Long id;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
	@NotBlank(message = "El nombre tipo de medicamento no puede quedar vacío")
	@Basic(optional = false)
	@Column(name = "NombreTipoMedicamento", unique = true)
	private String nombreTipoMedicamento;

	@NotNull(message = "El estado tipo medicamento no puede ser null")
	@Basic(optional = false)
	@Column(name = "EstadoTipoMedicamento")
	private Boolean estadoTipoMedicamento;

	@JsonIgnore
	@OneToMany(mappedBy = "idTipoMedicamento")
	private List<Medicamento> medicamentoList;

	public TipoMedicamento(Long id,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El nombre tipo de medicamento no puede quedar vacío") String nombreTipoMedicamento,
			@NotNull(message = "El estado tipo medicamento no puede ser null") Boolean estadoTipoMedicamento) {
		super();
		this.id = id;
		this.nombreTipoMedicamento = nombreTipoMedicamento;
		this.estadoTipoMedicamento = estadoTipoMedicamento;
	}

	public TipoMedicamento() {
		super();
	}
	
}
