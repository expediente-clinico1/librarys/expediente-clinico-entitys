package expendiente.clinico.models.entitys;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Entity
@Table(name = "EspecialidadMedica", schema = "clinica")
public class EspecialidadMedica implements Serializable {

	private static final long serialVersionUID = 430998763L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "Id")
	private Long id;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890 ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
	@NotBlank(message = "El NombreEspecialidadMedica no puede quedar vacio")
	@Basic(optional = false)
	@Column(name = "NombreEspecialidadMedica", unique = true)
	private String nombreEspecialidadMedica;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,126}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 127 caracteres")
	@Column(name = "DescripcionEspecialidadMedica")
	private String descripcionEspecialidadMedica;

	@NotNull(message = "El estado EstadoEspecialidadMedica no puede ser null")
	@Basic(optional = false)
	@Column(name = "EstadoEspecialidadMedica")
	private Boolean estadoEspecialidadMedica;

	public EspecialidadMedica(Long id,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890 ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El NombreEspecialidadMedica no puede quedar vacio") String nombreEspecialidadMedica,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,126}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 127 caracteres") String descripcionEspecialidadMedica,
			@NotNull(message = "El estado EstadoEspecialidadMedica no puede ser null") Boolean estadoEspecialidadMedica) {
		super();
		this.id = id;
		this.nombreEspecialidadMedica = nombreEspecialidadMedica;
		this.descripcionEspecialidadMedica = descripcionEspecialidadMedica;
		this.estadoEspecialidadMedica = estadoEspecialidadMedica;
	}

	public EspecialidadMedica() {
		super();
	}

}
