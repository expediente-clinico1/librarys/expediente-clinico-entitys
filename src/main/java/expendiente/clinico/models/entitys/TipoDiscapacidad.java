package expendiente.clinico.models.entitys;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Entity
@Table(name = "TipoDiscapacidad", schema = "catalogo")
public class TipoDiscapacidad implements Serializable {

	private static final long serialVersionUID = -396839900L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "IdTipoDiscapacidad")
	private Long id;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
	@NotBlank(message = "El nombre de TipoDiscapacidad no puede quedar vacio")
	@Basic(optional = false)
	@Column(name = "NombreTipoDiscapacidad")
	private String nombreTipoDiscapacidad;

	@NotNull(message = "El estado TipoDiscapacidad no puede ser null")
	@Basic(optional = false)
	@Column(name = "EstadoTipoDiscapacidad")
	private Boolean estadoTipoDiscapacidad;
	
	// @OneToMany(mappedBy = "idDiscapacidad")
	// private List<DiscapacidadExpedienteMedico> discapacidadList;

	public TipoDiscapacidad(Long id,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El nombre de TipoDiscapacidad no puede quedar vacio") String nombreTipoDiscapacidad,
			@NotNull(message = "El estado TipoDiscapacidad no puede ser null") Boolean estadoTipoDiscapacidad) {
		super();
		this.id = id;
		this.nombreTipoDiscapacidad = nombreTipoDiscapacidad;
		this.estadoTipoDiscapacidad = estadoTipoDiscapacidad;
	}

	public TipoDiscapacidad() {
		super();
	}
	
}
