package expendiente.clinico.models.entitys;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Entity
@Table(name = "TipoTratamiento", schema = "catalogo")
public class TipoTratamiento implements Serializable {

	private static final long serialVersionUID = 601690538L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "IdTipoTratamiento")
	private Long id;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
	@NotBlank(message = "El nombre del tipo tratamiento no puede quedar vacio")
	@Basic(optional = false)
	@Column(name = "NombreTratamiento", unique = true)
	private String nombreTratamiento;

	@NotNull(message = "El estado del tipo tratamiento no debe de ser null")
	@Basic(optional = false)
	@Column(name = "EstadoTratamiento")
	private Boolean estadoTratamiento;

	public TipoTratamiento(Long id,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El nombre del tipo tratamiento no puede quedar vacio") String nombreTratamiento,
			@NotNull(message = "El estado del tipo tratamiento no debe de ser null") Boolean estadoTratamiento) {
		super();
		this.id = id;
		this.nombreTratamiento = nombreTratamiento;
		this.estadoTratamiento = estadoTratamiento;
	}

	public TipoTratamiento() {
		super();
	}

}
